//1. Lopping dengan while
console.log('LOOPING PERTAMA')
var loop = 2 ;
while(loop <= 20){
  console.log(loop + ' I love coding');
loop++
}
console.log('LOOPING KEDUA')
while(loop >= 2){
  console.log(loop + ' I will become nodejs developer');
loop--
}


//2. Lopping dengan For
function loopdenganfor() {
  let bilgenap = []
  for (let i = 1; i <= 20; i++) {
      if(i<=20){
          bilgenap.push(i + '- I Love Coding')
      }
      
  }
  return bilgenap
}

// 3. Angka Ganjil Genap dan Kelipatan

// Angka Ganjil
function panggilbilganjil() {
  let bilganjil = []
  for (let i  = 1; i  < 100;i++){
      if(i%2 ===1){
          bilganjil.push(i + '- Angka Ganjil')
      }
      
  }
  return bilganjil
}
// Angka Genap
function panggilbilgenap() {
  let bilgenap = []
  for (let i = 1; i < 100; i++) {
      if(i%2 ===0){
          bilgenap.push(i + '- Angka Genap')
      }
      
  }
  return bilgenap
}

// Angka Kelipatan 3
function kelipatan3() {
  let data = []
for (let i = 1; i < 100; i++){
  if (i%3 === 0) {
      data.push(i + '- Bilangan Kelipatan 3')
  }  
}
return data;
}

// Angka Kelipatan 6
function kelipatan6() {
  let data2 = []
for (let i = 1; i < 100; i++){
  if (i%6 === 0) {
      data2.push(i + '- Bilangan Kelipatan 6')
  }  
}
return data2;
} 

// Angka Kelipatan 10
function kelipatan10() {
  let data3 = []
for (let i = 1; i < 100; i++){
  if (i%10 === 0) {
      data3.push(i + '- Bilangan Kelipatan 10')
  }  
}
return data3;
} 


console.log(loopdenganfor())
console.log(panggilbilgenap())
console.log(panggilbilganjil())
console.log(kelipatan3())
console.log(kelipatan6())
console.log(kelipatan10())
