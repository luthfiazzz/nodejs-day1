// 1. Menggunakan If Else

var nama = 'luthfi';
var peran = 'Ksatria';

if (nama !== '' && peran == "Ksatria") {
  console.log("Selamat datang di dunia Proxytia, " + nama);
  console.log("Halo Ksatria " + nama + ", kamu dapat menyerang dengan senjatamu!");
}

else if (nama !== '' && peran == "Tabib") {
  console.log("Selamat datang di dunia Proxytia, " + nama);
  console.log("Halo Tabib " + nama + ", kamu akan membantu temanmu yang terluka.");
}

else if (nama !== '' && peran == "Penyihir") {
  console.log("Selamat datang di dunia Proxytia, " + nama);
  console.log("Halo Penyihir " + nama + ", ciptakan keajaiban yang membantu kemenanganmu!");
}

else if (nama !== '')
  console.log("Halo " + nama + ", pilih peranmu untuk memulai game.");

else 
  console.log("Nama harus diisi!");


//   2. Menggunakan Switch-Case

var tanggal = 1;
var bulan = 5;
var tahun = 1945;

switch(bulan) {
  case 1: { console.log(tanggal + ' Januari ' + tahun); break; }
  case 2: { console.log(tanggal + ' Februari ' + tahun); break; }
  case 3: { console.log(tanggal + ' Maret ' + tahun); break; }
  case 4: { console.log(tanggal + ' April ' + tahun); break; }
  case 5: { console.log(tanggal + ' Mei ' + tahun); break; }
  case 6: { console.log(tanggal + ' Juni ' + tahun); break; }  
  case 7: { console.log(tanggal + ' Juli ' + tahun); break; }
  case 8: { console.log(tanggal + ' Agustus ' + tahun); break; }
  case 9: { console.log(tanggal + ' September ' + tahun); break; }
  case 10: { console.log(tanggal + ' Oktober ' + tahun); break; }
  case 11: { console.log(tanggal + ' November ' + tahun); break; }
  case 12: { console.log(tanggal + ' Desember ' + tahun); break; }
  default : { console.log('Bulan tidak tersedia'); }
}

var tanggal; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
