//bagia A

var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

var gabung = word+ ' ' + second+ ' ' + third+ ' ' + fourth+ ' ' + fifth+ ' ' + sixth+ ' ' + seventh+ ' ';
console.log(gabung);


// bagian B
var word = 'wow JavaScript is so cool';
var exampleFirstWord = word[0] + word[1] + word[2];
var secondWord = word[4] + word[5] + word[6] + word[7] + word[8] + word[9] + word[10] + word[11] + word[12] + word[13]; 
var thirdWord = word[15] + word[16];
var fourthWord = word[18] + word[19];
var fifthWord = word[21] + word[22] + word[23] + word[24];

console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: ' + secondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);

// bagian C
var word3 = 'wow JavaScript is so cool';
var exampleFirstWord3 = word3.substring(0, 3);
var secondWord3 = word3.substring(4, 14); 
var thirdWord3 = word3.substring(15, 17); 
var fourthWord3 = word3.substring(18, 20); 
var fifthWord3 = word3.substring(21, 24); 

console.log('First Word: ' + exampleFirstWord3);
console.log('Second Word: ' + secondWord3);
console.log('Third Word: ' + thirdWord3);
console.log('Fourth Word: ' + fourthWord3);
console.log('Fifth Word: ' + fifthWord3);

// bagian D

var word4 = 'wow JavaScript is so cool';
var exampleFirstword4 = word4.substring(0, 3);
var secondword4 = word4.substring(4, 14); 
var thirdword4 = word4.substring(15, 17); 
var fourthword4 = word4.substring(18, 20); 
var fifthword4 = word4.substring(21, 25); 

var firstWordLength = exampleFirstword4.length;
var secondWordLength = secondword4.length;
var thirdWordLength = thirdword4.length;
var fourWordLength = fourthword4.length;
var fifthtWordLength = fifthword4.length;
// create new variables around here

console.log('First Word: ' + exampleFirstword4 + ', with length: ' + firstWordLength);
console.log('Second Word: ' + secondword4 + ', with length: ' + secondWordLength);
console.log('Third Word: ' + thirdword4 + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthword4 + ', with length: ' + fourWordLength);
console.log('Fifth Word: ' + fifthword4 + ', with length: ' + fifthtWordLength);


